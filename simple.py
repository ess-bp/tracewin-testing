'''
Downloads latest version from TraceWin
and test that one..
'''

if __name__=="__main__":
    import tests,sys

    if len(sys.argv)>1:
        for test in sys.argv[1:]:
            tests.run_test_name(test, 'TraceWin', ["calc"])
    else:
        tests.run_test_name("first_test", 'TraceWin', ["calc"])
        tests.run_test_name("dipole_edge", 'TraceWin', ["calc"])
        tests.run_test_name("dtl_errors", 'TraceWin', ["calc"])
        tests.run_test_name("set_beam_phs_err", 'TraceWin', ["calc"])
        tests.run_test_name("scc_fieldmap", 'TraceWin', ["calc"])
        tests.run_test_name("fieldmap_diag", 'TraceWin', ["calc"])
        tests.run_test_name("mini_rfq", 'TraceWin', ["calc"])
        tests.run_test_name("quick_errstudy", 'TraceWin', ["calc"])
