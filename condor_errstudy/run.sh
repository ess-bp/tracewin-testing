#!/bin/sh

# input:
# - OpSys
# - Arch
# - RandInt

if [ $# -ne 3 ]
then
	echo "Usage : $0 OpSys Arch RandInt"
	exit 1
fi

if [ "$1" == "LINUX" ]
then
	if [ "$2" == "X86_64" ]
	then
		echo "Linux, 64 bit"
		TWIN="tracelx64"
	else
		echo "Wrong Arch: $2"
		exit 1
	fi
elif [ "$1" == "OSX" ]
then
	if [ "$2" == "X86_64" ]
	then
		echo "OSX, 64 bit"
		TWIN="tracemac64"
	else
		echo "Wrong Arch: $2"
		exit 1
	fi
else
	echo "ERROR, wrong OS: $1"
	exit 1
fi

./$TWIN 0.2 14 $3

ls -gh

mkdir data
mv Density_PAR.dat_0 \
   ENV_diag1.dat \
   Error_Datas.txt \
   PAR_diag1.dat \
   Remote_DATA.dat_0 \
   dtl1.plt \
   lattice.par \
   part_dtl1.dst_0 \
   part_rfq.dst \
   data/
