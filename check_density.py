from ess import TraceWin
import os
import sys

calc_dir = os.path.join(sys.argv[1], 'calc')

for f in os.listdir(calc_dir):
    if 'Density_PAR' in f:
        fpath = os.path.join(calc_dir, f)
        print(fpath)
        den = TraceWin.density_file(fpath)
        print(f"{f} has version {den.version}")

