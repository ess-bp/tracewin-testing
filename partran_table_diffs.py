#!/usr/bin/env python

import sys
from matplotlib import pyplot as plt
from ess import TraceWin

# Usage: ./script.py partran1.dat partran2.dat
# Can also be used to compare TW tables
f1 = sys.argv[1]
f2 = sys.argv[2]

d1 = TraceWin.partran(f1)
d2 = TraceWin.partran(f2)

for key in d1:
    if d1[key][-1] != d2[key][-1]:
        print(key, d1[key][-1], d2[key][-1])
        #continue
        plt.figure()
        plt.title(key)
        plt.plot(d1['z(m)'], d1[key], 'x-', label=f1)
        plt.plot(d2['z(m)'], d2[key], '.-', label=f2)
        plt.legend()

plt.show()
