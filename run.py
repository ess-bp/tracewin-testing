from __future__ import print_function

import os, shutil, stat

import tests



SOURCES_DIR = "/storage/public/cea_sources/"
FILE_LIST = [
    "TraceWin64_noX11_",
    "TraceWin_",
    "TraceWin_",
    "TraceWin_",
    "twserver_without_X_",
]
FILE_END = [
        "",
        ".dmg",
        "",
        ".exe",
        "",
    ]

TMP_DIR="temp/"

def get_version():
    version=file('VERSION','r').read()
    return version.strip()

def check_files_exist(version):
    for f, f_end in zip(FILE_LIST,FILE_END):
        filepath=os.path.join(SOURCES_DIR,f+version+f_end)
        if not os.path.exists(filepath):
            print("Could not find ",filepath)
            return 1
    return 0

def setup_tempdir():
    if os.path.isdir(TMP_DIR):
        shutil.rmtree(TMP_DIR)
    os.mkdir(TMP_DIR)

def get_tw_executable(version):
    '''
    Should check platform and
    return the TW executable for
    that platform...
    '''
    return os.path.join(SOURCES_DIR,"TraceWin64_noX11_"+version)

def delete_all_config():
    '''
    In order to make sure problems don't arise
    due to incorrect config files, we should delete
     ~/TraceWin_exe
     ~/.config/CEA
     ~/TraceWin_doc
    '''
    HOME=os.environ.get("HOME")

    TraceWin_exe=os.path.join(HOME,"TraceWin_exe")
    if os.path.isdir(TraceWin_exe):
        print("Deleting",TraceWin_exe)
        shutil.rmtree(TraceWin_exe)

def copy_tw_executable(path):
    new_path=os.path.join(TMP_DIR,"TraceWin")
    shutil.copy(path,new_path)

    # set executable mode:
    old_mode=os.stat(new_path).st_mode
    new_mode = old_mode | stat.S_IXUSR
    new_mode = new_mode | stat.S_IXGRP
    new_mode = new_mode | stat.S_IXOTH
    os.chmod(new_path,new_mode)

    return new_path

if __name__=="__main__":
    version=get_version()
    print("Will test version",version)
    check_files_exist(version)

    delete_all_config()
    setup_tempdir()
    tw_exec=get_tw_executable(version)
    tw_exec=copy_tw_executable(tw_exec)


    tests.run_test_name("first_test", tw_exec, ["calc"])
    tests.run_test_name("dtl_errors", tw_exec, ["calc"])
    tests.run_test_name("quick_errstudy", tw_exec, ["calc"])
    tests.run_test_name("set_beam_phs_err", tw_exec, ["calc"])
    tests.run_test_name("scc_fieldmap", tw_exec, ["calc"])
    #tests.run_test_name("condor_errstudy", tw_exec, ["data_0", "data_1"], is_condor=True, check_output=False)

