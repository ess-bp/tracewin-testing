from __future__ import print_function

import os, sys, subprocess
import time

def _run_simple_test( test_name, folders, tw_executable):

    if not os.path.isdir(test_name):
        raise ValueError("Could not find test "+test_name)

    cmd = [os.path.join("..",tw_executable)]
    cmd.append('project.ini')
    cmd.append('hide_esc')
    if has_synoptic(test_name, folders):
        cmd.append("synoptic_file={}".format(has_synoptic(test_name, folders)))
    os.chdir(test_name)
    cmd=" ".join(cmd)
    print(cmd)
    subprocess.call(cmd, shell=True,stdout=open('calc/tw.txt','w'))
    os.chdir("..")

def _run_extra(test_name):
    '''
    Runs extra code to produce output if needed
    '''

    os.chdir(test_name)

    if os.path.isfile('extra.py'):
        exit=subprocess.call('python extra.py', shell=True)
        if exit:
            raise ValueError("Running extra script failed, exit value {}".format(exit))

    os.chdir("..")

def _run_condor_test(test_name,tw_executable):
    '''
    Not yet implemented...
    '''
    pass

def _compare_files(f1, f2, cfg=''):

    import subprocess

    if not os.path.exists(f2):
        raise ValueError(f"Diff not possible, {f2} missing")
    try:
        stdout=subprocess.check_output(f"./numdiff {f1} {f2} {cfg}", shell=True, stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError as e:
        raise ValueError(f"Command: {e.cmd}, \n  exited with non-zero output value {e.returncode}, \n {e.output}".format(e.cmd, e.returncode, e.output))
    if b'do not differ' in stdout:
        print(f"File accepted: {f2}")
        return 0.0
    else:
        print(stdout.decode())
        return 1.0

def _check_correct_output(test_name, folders):
    errors=[]
    for folder in folders:
        print("Checking output in ",folder)
        fpath=os.path.join(test_name,folder)
        fpath_corr=os.path.join(test_name,folder+"_correct")
        if not os.path.isdir(fpath):
            print("Missing calculation folder")
        if not os.path.isdir(fpath_corr):
            print("Missing reference calculation folder")
        for f in os.listdir(fpath_corr):
            if f.split('.')[-1]!='cfg':
                f_ref=os.path.join(fpath_corr,f)
                f_new=os.path.join(fpath,f)
                f_cfg=f_ref+'.cfg'
                if not os.path.exists(f_cfg):
                    f_cfg=''
                ratio=_compare_files(f_ref, f_new, f_cfg)
                if ratio:
                    errors.append([f_new,ratio])
        if errors:
            for e in errors:
                print(f"Ratio {e[0]} too large for {e[1]}")
            raise ValueError(f"There were {len(errors)} files failing the comparison test")

def has_synoptic( test_name, folders):
    for f in folders:
        path = os.listdir(os.path.join( test_name, f + '_correct'))
        for filename in path:
            if 'geometric_layout_middle_pos.dat' in filename:
                return 2
    return 0

def run_test_name(test_name,tw_executable, folders, is_condor=False, check_output=True):
    print("Running test",test_name)
    start=time.time()
    if is_condor:
        _run_condor_test(test_name, tw_executable)
    else:
        _run_simple_test(test_name, folders, tw_executable)
    end=time.time()
    print("Time elapsed for test: {}:{:.2f}".format(int(end-start)/60,(end-start)%60))
    _run_extra(test_name)
    if check_output:
        _check_correct_output(test_name,folders)

