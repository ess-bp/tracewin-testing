TraceWin test scripts for ESS
-----------------------------

To test a specific version on our workstation, set the version number in VERSION, and then run 'python run.py'

The simple.py script is used to automatically download and run the latest TraceWin from the server. This is run every time there is a change to the repository, but also every 72h since TraceWin might change without this repository changing.


[![build status](https://gitlab01.esss.lu.se/ess-bp/tracewin-testing/badges/master/build.svg)](https://gitlab01.esss.lu.se/ess-bp/tracewin-testing/builds)

